const { assert } = require('chai');
const { newUser } = require('./../index.js');

describe('Test newUser object', () => { //test suite
/*	it('Assert newUser type is an object', () => { //test case
		assert.equal(typeof(newUser), 'object')
	});

	it('Assert newUser email is type string', () => {
		assert.equal(typeof(newUser.email), 'string')
	});

	it('Assert newUser email is type string', () => {
		assert.notEqual(typeof(newUser.email), 'undefined')
	});

	it('Assert newUser password is type string', () => {
		assert.equal(typeof(newUser.password), 'string')
	});

	it('Password at least 16 char', () => {
		assert.isAtLeast(newUser.password.length, 16)
	});*/
	///activity
	it('Assert newUser lastName is type string', () => {
		assert.equal(typeof(newUser.firstName), 'string')
	});
		it('Assert newUser firstName is not undefined', () => {
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	});
		it('Assert newUser age is at least 18', () => {
		assert.isAtLeast(newUser.age, 18)
	});	
		it('Assert newUser age is a number', () => {
		assert.isAtLeast(typeof(newUser.age), "number")
	});		
		it('Assert newUser contact is a string', () => {
		assert.isAtLeast(typeof(newUser.contactNo), "string")
	});				
		it('Assert newUser batch num is a number', () => {
		assert.isAtLeast(typeof(newUser.batchNo), "number")
	});		
		it('Assert newUser batch num is not undefined', () => {
		assert.notEqual(typeof(newUser.lastName), 'undefined')
	});
});